# Tradaru Shop

## Fetching product from API
![trada1](/uploads/d42188e20d27b9d16f3ed1e84156ed2f/trada1.PNG)
## Add to Favorite
![trada2](/uploads/0b59f062e95a26c0c3ecde0c79a12e13/trada2.PNG)
## Details Product
![trada3](/uploads/b07b0ea32bd0a8347ce31a163717c087/trada3.PNG)
## Cart Product selected
![trada4](/uploads/f7dc2c31fd98442c5a3e35d572cc4d8f/trada4.PNG)
## Favorite product selected , delete handling
![trada5](/uploads/4f669fac87d9c7e79a40398f2e53fe9f/trada5.PNG)
## Add quantity product
![trada6](/uploads/d637601ea80f17f41dfe44b6b51be982/trada6.PNG)
## chech out product
![trada7](/uploads/f121527d9212e80e7c3abcf6decd66f6/trada7.PNG)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
