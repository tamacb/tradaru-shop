import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tadarushop/view/cart/cart_controller/cart_controller.dart';
import 'package:get/get.dart';
import 'package:tadarushop/view/widget/checkout_button.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  CartController _cartController = Get.put(CartController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Get.toNamed('/')),
      ),
      body: (_cartController.isLoading.value)
          ? LinearProgressIndicator()
          : ListView.builder(
        itemCount: _cartController.carts.length,
        itemBuilder: (context, index) => Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Container(
                    child: Image.network(
                        'https://image.freepik.com/free-psd/paper-coffee-bags-mockup_58466-11166.jpg',fit: BoxFit.cover,),
                    width: MediaQuery.of(context).size.width * 0.30,
                    height:
                    (_cartController.carts[index].title.length >=
                        35)
                        ? MediaQuery.of(context).size.height * 0.25
                        : MediaQuery.of(context).size.height * 0.15,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          bottomLeft: Radius.circular(8)),
                      color: Colors.black12,
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            _cartController.carts[index].title,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 16.0),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 8,
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            children: [
                              Center(
                                  child: Text(
                                    NumberFormat.currency(
                                        locale: 'id',
                                        symbol: 'Rp ',
                                        decimalDigits: 0)
                                        .format(_cartController
                                        .carts[index].price),
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.0),
                                  )),
                              Center(
                                child: Row(
                                  children: [
                                    IconButton(
                                        icon: Icon(Icons.remove),
                                        onPressed: (){
                                          setState(() {
                                            (_cartController
                                                .carts[index].quantity == 1) ? 1 :
                                            _cartController
                                                .carts[index].quantity--;
                                          });
                                        }
                                    ),
                                    Text(_cartController
                                        .carts[index].quantity
                                        .toString()),
                                    IconButton(
                                        icon: Icon(Icons.add),
                                        onPressed: (){
                                          setState(() {
                                            _cartController
                                                .carts[index].quantity++;
                                          });
                                        }
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceAround,
                            children: [
                              Center(
                                  child: Text(
                                    'baru',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.0),
                                  )),
                              Center(
                                  child: Text(
                                    _cartController
                                        .carts[index].weight +
                                        ' Kg',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.0),
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ),
                    width: MediaQuery.of(context).size.width * 0.63,
                    height:
                    (_cartController.carts[index].title.length >=
                        35)
                        ? MediaQuery.of(context).size.height * 0.25
                        : MediaQuery.of(context).size.height * 0.15,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          bottomRight: Radius.circular(8)),
                      color: Colors.white10,
                    ),
                  ),
                ],
              ),
            )),
      ),
      bottomNavigationBar: _cartController.carts.length == 0
          ? Center(
        child: Text(
          'carts kosong, belanja sekarang',
          style: TextStyle(
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.italic),
        ),
      )
          : Container(
        padding: EdgeInsets.all(25),
        decoration: BoxDecoration(
            color: Colors.pink[50],
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(13),
                topLeft: Radius.circular(13))),
        child: SafeArea(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "TOTAL",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      NumberFormat.currency(
                          locale: 'id',
                          symbol: 'Rp. ',
                          decimalDigits: 0)
                          .format(_cartController.totalPrice),
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: CheckoutButton(
                  _cartController.checkOut,
                  "CHECKOUT",
                  Icons.check,
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
