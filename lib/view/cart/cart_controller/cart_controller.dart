import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tadarushop/model/product_model.dart';

class CartController extends GetxController {
  final isLoading = true.obs;
  var carts = List<ProductModel>().obs;

  int get countItem => carts.length;
  double get totalPrice =>
      carts.fold(0, (sum, item) => sum + item.price * item.quantity.toDouble());

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchCarts();
  }

  Future fetchCarts() async {
    try {
      isLoading(true);
      List<ProductModel> cartResult = [];
      carts.value = cartResult;
    } catch (e) {
      print(e);
    } finally {
      isLoading(false);
    }
  }

  addCart(ProductModel productModel) async {
    try {
      await Future.delayed(Duration(milliseconds: 10))
          .then((value) => carts.add(productModel));
      Get.offNamed('/');
    } catch (e) {
      print(e);
    } finally {}
  }

  checkOut() {
    carts.clear();
    Get.offAndToNamed("/");
    Get.snackbar(
      "Placed",
      "Order placed with success!",
      backgroundColor: Color(0xff3a3a3a),
      colorText: Color(0xffffffff),
      padding: EdgeInsets.all(15),
      snackPosition: SnackPosition.BOTTOM,
      margin: EdgeInsets.all(25),
      icon: Icon(Icons.check_circle, color: Colors.greenAccent, size: 21),
    );
  }
}