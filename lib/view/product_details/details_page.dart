import 'package:flutter/material.dart';
import 'package:tadarushop/model/product_model.dart';
import 'package:get/get.dart';
import 'package:tadarushop/view/cart/cart_controller/cart_controller.dart';
import 'package:tadarushop/view/favorite/favorite_product/favorite_controller.dart';

class DetailsPage extends StatefulWidget {
  final ProductModel productModel;

  DetailsPage({this.productModel});

  @override
  _DetailsPageState createState() => _DetailsPageState(productModel);
}

class _DetailsPageState extends State<DetailsPage> {
  final ProductModel productModel;

  _DetailsPageState(this.productModel);

  final FavoriteController _favoriteController = Get.put(FavoriteController());
  final CartController _cartController = Get.put(CartController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.pink[50],
            ),
            SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Center(
                          child: IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: () => Get.back(),
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.favorite_border),
                          onPressed: () async => await _favoriteController
                              .addFavorite(productModel),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.45,
                    color: Colors.pink[50],
                    child: Center(
                        child: Column(
                      children: [
                        Image.network(
                          'https://image.freepik.com/free-psd/paper-coffee-bags-mockup_58466-11166.jpg',
                          fit: BoxFit.fill,
                        ),
                        Text(productModel.id),
                      ],
                    )),
                  ),
                  Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height * 0.6,
                    color: Colors.white,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment:
                                MainAxisAlignment.spaceAround,
                            children: [
                              Icon(Icons.verified_user),
                              Text(
                                productModel.title,
                                maxLines: 3,
                                style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              height: 100.0,
                              width: double.infinity,
                              child: Text(productModel.description)),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Item Size',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 15),
                              )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                  height: 30.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.pinkAccent[100]),
                                  child: Center(
                                      child: Text('S',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 15)))),
                              Container(
                                  height: 30.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.pinkAccent[100]),
                                  child: Center(
                                      child: Text('M',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 15)))),
                              Container(
                                  height: 30.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.pinkAccent[100]),
                                  child: Center(
                                      child: Text('L',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 15)))),
                              Container(
                                  height: 30.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.pinkAccent[100]),
                                  child: Center(
                                      child: Text('XL',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 15)))),
                              Container(
                                  height: 30.0,
                                  width: 30.0,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.pinkAccent[100]),
                                  child: Center(
                                      child: Text('XXL',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 15)))),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Item Colors',
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 15),
                              )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                height: 30.0,
                                width: 30.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Colors.black),
                              ),
                              Container(
                                height: 30.0,
                                width: 30.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Colors.amber),
                              ),
                              Container(
                                height: 30.0,
                                width: 30.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Colors.blue),
                              ),
                              Container(
                                height: 30.0,
                                width: 30.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Colors.purple),
                              ),
                              Container(
                                height: 30.0,
                                width: 30.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Colors.green),
                              ),
                              Container(
                                height: 30.0,
                                width: 30.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50.0),
                                    color: Colors.pinkAccent),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Align(
                            alignment: Alignment.bottomLeft,
                            child: Text(
                              'Harga : Rp.  ' + productModel.price.toString(),
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(
            right: 10.0, left: 10.0, top: 10.0, bottom: 10.0),
        child: GestureDetector(
          onTap: () async => await _cartController.addCart(productModel),
          child: Container(
            width: double.infinity,
            height: 45.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: Colors.pinkAccent,
            ),
            child: Center(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.add_shopping_cart),
                Text(
                  'Add to cart',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                ),
              ],
            )),
          ),
        ),
      ),
    );
  }
}
