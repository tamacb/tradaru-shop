import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:tadarushop/view/cart/cart_controller/cart_controller.dart';
import 'package:tadarushop/view/favorite/favorite_product/favorite_controller.dart';
import 'package:tadarushop/view/product/product_controller/product_controller.dart';
import 'package:tadarushop/view/product_details/details_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ProductController _productController = Get.put(ProductController());
  final FavoriteController _favoriteController = Get.put(FavoriteController());
  final CartController _cartController = Get.put(CartController());

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          body: SafeArea(
            child: Stack(
              children: [
                Container(
                  height: double.infinity,
                  width: double.infinity,
                  color: Colors.pink[50],
                ),
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              height: 50.0,
                              width: 50.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50.0),
                                  color: Colors.pinkAccent),
                              child: Center(
                                child: Text(
                                  'TdrX',
                                  style: TextStyle(
                                      fontFamily: 'avenir',
                                      fontSize: 10,
                                      fontWeight: FontWeight.w900),
                                ),
                              ),
                            ),
                            IconButton(
                              icon: Stack(
                                children: [
                                  Icon(
                                    Icons.shopping_cart,
                                    size: 35,
                                  ),
                                  Positioned(
                                    top: 1,
                                    child: Container(
                                      height: 18.0,
                                      width: 18.0,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50.0),
                                          color: Colors.pink),
                                      child: Center(
                                        child: Text(
                                          _cartController.carts.length
                                              .toString(),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 10),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              onPressed: () => Get.toNamed('/cart'),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 2.0, left: 8.0),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Everythings Shop',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 19),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 2),
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Get Popular what do you want from home',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 12),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, top: 10, bottom: 10.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15)),
                          width: double.infinity,
                          height: 50.0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                child: IconButton(
                                  icon: Icon(Icons.search),
                                  onPressed: null,
                                ),
                                flex: 1,
                              ),
                              Flexible(
                                child: TextFormField(
                                  cursorColor: Colors.black,
                                  decoration: new InputDecoration(
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      contentPadding: EdgeInsets.only(
                                          left: 15.0,
                                          bottom: 8.0,
                                          top: 8.0,
                                          right: 15.0),
                                      hintText: "Look for everything you need"),
                                ),
                                flex: 3,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 2.0, left: 20.0, right: 20.0, bottom: 2.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Category',
                              style: TextStyle(
                                  fontSize: 19, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              'see all',
                              style: TextStyle(color: Colors.red),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: Container(
                          width: double.infinity,
                          height: 80.0,
                          child: ListView(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.pinkAccent[100],
                                  ),
                                  height: 40.0,
                                  width: 80.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart),
                                      Text('Makanan')
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.pinkAccent[100],
                                  ),
                                  height: 40.0,
                                  width: 80.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart),
                                      Text('Minuman')
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.pinkAccent[100],
                                  ),
                                  height: 40.0,
                                  width: 80.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart),
                                      Text('Obat')
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.pinkAccent[100],
                                  ),
                                  height: 40.0,
                                  width: 80.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart),
                                      Text('Baju')
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.pinkAccent[100],
                                  ),
                                  height: 40.0,
                                  width: 80.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart),
                                      Text('Elektronik')
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.pinkAccent[100],
                                  ),
                                  height: 40.0,
                                  width: 80.0,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart),
                                      Text('Otomotif')
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, bottom: 4.0, top: 4.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Popular Sugestions',
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              'see all',
                              style: TextStyle(color: Colors.red),
                            ),
                          ],
                        ),
                      ),
                      (_productController.isLoading.value)
                          ? LinearProgressIndicator()
                          : Container(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: StaggeredGridView.countBuilder(
                                crossAxisCount: 2,
                                itemCount: _productController.countItemProduct,
                                crossAxisSpacing: 16,
                                mainAxisSpacing: 16,
                                staggeredTileBuilder: (index) =>
                                    StaggeredTile.fit(1),
                                itemBuilder: (context, index) {
                                  return Card(
                                    color: Colors.red[50],
                                    elevation: 2,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Stack(
                                            children: [
                                              Container(
                                                height: 100,
                                                width: double.infinity,
                                                clipBehavior: Clip.antiAlias,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(4),
                                                ),
                                                child: GestureDetector(
                                                  onTap: () =>
                                                      Get.to(DetailsPage(
                                                    productModel:
                                                        _productController
                                                            .productList[index],
                                                  )),
                                                  child: Image.network(
                                                    "https://image.freepik.com/free-psd/paper-coffee-bags-mockup_58466-11166.jpg",
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  SizedBox(
                                                    width: 40,
                                                    height: 40,
                                                    child: FlatButton(
                                                      padding: EdgeInsets.zero,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          topRight:
                                                              Radius.circular(
                                                                  8),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  8),
                                                        ),
                                                      ),
                                                      child: Icon(
                                                        Icons.favorite_border,
                                                        size: 21,
                                                      ),
                                                      onPressed: () async =>
                                                          await _favoriteController
                                                              .addFavorite(
                                                                  _productController
                                                                          .productList[
                                                                      index]),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 10.0),
                                                    child: GestureDetector(
                                                        onTap: () async =>
                                                            await _cartController
                                                                .addCart(
                                                                    _productController
                                                                            .productList[
                                                                        index]),
                                                        child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius: BorderRadius.only(
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          5)),
                                                              color: Colors
                                                                  .white70,
                                                              boxShadow: [
                                                                BoxShadow(
                                                                  color: Colors
                                                                      .grey
                                                                      .withOpacity(
                                                                          0.5),
                                                                  spreadRadius:
                                                                      3,
                                                                  blurRadius: 5,
                                                                  offset: Offset(
                                                                      0,
                                                                      3), // changes position of shadow
                                                                ),
                                                              ],
                                                            ),
                                                            height: 30.0,
                                                            width: 30.0,
                                                            child: Icon(
                                                              Icons
                                                                  .shopping_cart,
                                                              color:
                                                                  Colors.pink,
                                                            ))),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                          SizedBox(height: 8),
                                          GestureDetector(
                                            onTap: () => Get.to(DetailsPage(
                                              productModel: _productController
                                                  .productList[index],
                                            )),
                                            child: Text(
                                              _productController
                                                  .productList[index].title,
                                              maxLines: 8,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w800),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          Text(
                                            NumberFormat.currency(
                                                    locale: 'id',
                                                    symbol: 'Rp ',
                                                    decimalDigits: 0)
                                                .format(_productController
                                                    .productList[index].price),
                                            maxLines: 8,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w800),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            )
                    ],
                  ),
                ),
              ],
            ),
          ),
          bottomNavigationBar: Container(
            color: Colors.white30,
            width: double.infinity,
            height: 60.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () => Get.toNamed('/'),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.home_outlined),
                      Text('Home'),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () => Get.toNamed('/favpage'),
                  child: Stack(
                    children: [
                      Positioned(
                        top: 8,
                        left: 1,
                        child: Container(
                          height: 18.0,
                          width: 18.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50.0),
                              color: Colors.pink),
                          child: Center(
                            child: Text(
                              _favoriteController.favorite.length.toString(),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10),
                            ),
                          ),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.favorite_border),
                          Text('Favorite'),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.history),
                    Text('History'),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.person_outline),
                    Text('Profile'),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
