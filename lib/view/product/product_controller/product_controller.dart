import 'package:get/get.dart';
import 'package:tadarushop/model/product_model.dart';
import 'package:tadarushop/repository/remote_repository.dart';

class ProductController extends GetxController {
  final RemoteRepository _repository = RemoteRepository();

  final isLoading = true.obs;
  var productList = List<ProductModel>().obs;
  int get countItemProduct => productList.length;

  void fetchProduct() async {
    try {
      isLoading(true);
      var products = await _repository.getProduct();
      if (products != null) {
        productList.value = products;
        print(productList);
      }
    } finally {
      isLoading(false);
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchProduct();
  }

}