import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:intl/intl.dart';
import 'package:tadarushop/view/cart/cart_controller/cart_controller.dart';
import 'package:tadarushop/view/favorite/favorite_product/favorite_controller.dart';
import 'package:tadarushop/view/product/product_controller/product_controller.dart';
import 'package:get/get.dart';
import 'package:tadarushop/view/product_details/details_page.dart';
class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  final FavoriteController _favoriteController = Get.put(FavoriteController());
  final CartController _cartController = Get.put(CartController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: ()=> Get.back(),
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              color: Colors.pink[50],
            ),
            (_favoriteController.countItemFavorite == 0) ? Center(child: Text('Cari barang yang kamu suka', style: TextStyle(fontWeight: FontWeight.bold))) : StaggeredGridView.countBuilder(
              crossAxisCount: 2,
              itemCount: _favoriteController.countItemFavorite,
              crossAxisSpacing: 16,
              mainAxisSpacing: 16,
              staggeredTileBuilder: (index) => StaggeredTile.fit(1),
              itemBuilder: (context, index) {
                return Card(
                  color: Colors.red[50],
                  elevation: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Stack(
                          children: [
                            Container(
                              height: 180,
                              width: double.infinity,
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                              ),
                              child: GestureDetector(
                                onTap: () => Get.to(DetailsPage(productModel: _favoriteController.favorite[index],)),
                                child: Image.network(
                                  "https://image.freepik.com/free-photo/top-view-arrangement-with-beauty-bag-copy-space_23-2148301851.jpg",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () async =>
                                  await _favoriteController
                                      .removeFavorite(_favoriteController
                                      .favorite
                                      .indexOf(_favoriteController
                                      .favorite[index])),
                                  child: SizedBox(
                                    width: 40,
                                    height: 40,
                                    child: FlatButton(
                                      padding: EdgeInsets.zero,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(8),
                                          bottomLeft: Radius.circular(8),
                                        ),
                                      ),
                                      child: Icon(
                                        Icons.delete_forever,
                                        size: 21,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: GestureDetector(
                                      onTap: () async =>
                                      await _cartController.addCart(
                                          _favoriteController.favorite[index]),
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(5)),
                                            color: Colors.white70,
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.grey.withOpacity(0.5),
                                                spreadRadius: 3,
                                                blurRadius: 5,
                                                offset: Offset(0,
                                                    3), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                          height: 30.0,
                                          width: 30.0,
                                          child: Icon(
                                            Icons.shopping_cart,
                                            color: Colors.pink,
                                          ))),
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(height: 8),
                        GestureDetector(
                          onTap: () => Get.to(DetailsPage(
                            productModel: _favoriteController.favorite[index],
                          )),
                          child: Text(
                            _favoriteController.favorite[index].title,
                            maxLines: 8,
                            style: TextStyle(fontWeight: FontWeight.w800),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Text(
                          NumberFormat.currency(
                              locale: 'id',
                              symbol: 'Rp ',
                              decimalDigits: 0)
                              .format(
                              _favoriteController.favorite[index].price),
                          maxLines: 8,
                          style: TextStyle(fontWeight: FontWeight.w800),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    ));
  }
}
