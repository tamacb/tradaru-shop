import 'package:get/get.dart';
import 'package:tadarushop/model/product_model.dart';

class FavoriteController extends GetxController {
  final isFavorite = true.obs;
  var isLoading = true.obs;
  var favorite = List<ProductModel>().obs;

  int get countItemFavorite => favorite.length;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchFavorite();
  }

  Future fetchFavorite() async {
    try {
      isLoading(true);
      List<ProductModel> favoriteResult = [];
      favorite.value = favoriteResult;
    } catch (e) {
      print(e);
    } finally {
      isLoading(false);
    }
  }

  addFavorite(ProductModel ProductModel) async {
    try {
      await Future.delayed(Duration(milliseconds: 30))
          .then((value) => favorite.add(ProductModel));
    } catch (e) {
      print(e);
    } finally {
    }
  }

  removeFavorite(int ProductModel) async {
    try {
      await Future.delayed(Duration(milliseconds: 30))
          .then((value) => favorite.removeAt(ProductModel));
    } catch (e) {
      print(e);
    } finally {}
  }
}