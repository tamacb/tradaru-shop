import 'package:http/http.dart' as http;
import 'package:tadarushop/model/product_model.dart';

class RemoteRepository {
  static var client = http.Client();

  var getProductUrl =
      'https://ranting.twisdev.com/index.php/rest/items/search/api_key/teampsisthebest/';

  Future<List<ProductModel>> getProduct() async {
    var response = await client.post(getProductUrl);
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return ProductModelFromJson(jsonString);
    } else {
      return null;
    }
  }
}
