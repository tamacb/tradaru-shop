import 'package:get/get.dart';
import 'package:tadarushop/view/cart/cart_page.dart';
import 'package:tadarushop/view/favorite/favorite_page.dart';
import 'package:tadarushop/view/product/product_page.dart';
import 'package:tadarushop/view/product_details/details_page.dart';

routes() => [
  GetPage(name: "/", page: () => HomePage()),
  GetPage(name: "/detailspage", page: () => DetailsPage()),
  GetPage(name: "/favpage", page: () => FavoritePage()),
  GetPage(name: "/cart", page: () => CartPage()),
];